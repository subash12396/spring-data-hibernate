package com.subi.springDataWithHibernate.Finder.Repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.subi.springDataWithHibernate.Finder.Entity.Item;

public interface ItemRepositiry extends PagingAndSortingRepository<Item, Integer> {
	
	List<Item> findByDescription(String description);
	
	List<Item> findByPrice(Double price);
	
	List<Item> findByNameAndPrice(String name, Double price);
	
	List<Item> findByPriceGreaterThan(Double price);
	
	List<Item> findByDescriptionContains(String desc);
	
	List<Item> findByPriceBetween(Double price1, Double price2);
	
	List<Item> findByNameLike(String name);

	List<Item> findByIdIn(List<Integer> ids,Pageable pageable);

}
