package com.subi.springDataWithHibernate.CRUD.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.CRUD.Entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}