package com.subi.springDataWithHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataWithHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataWithHibernateApplication.class, args);
	}

}
