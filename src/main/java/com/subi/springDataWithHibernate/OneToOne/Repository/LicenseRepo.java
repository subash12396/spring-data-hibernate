package com.subi.springDataWithHibernate.OneToOne.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.OneToOne.Entity.License;

public interface LicenseRepo extends CrudRepository<License, Integer> {

}
