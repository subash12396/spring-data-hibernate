package com.subi.springDataWithHibernate.IdGen.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Employee {


	@GenericGenerator(name ="emp_id",strategy = "com.subi.springDataWithHibernate.CustomerGenerator")
	@GeneratedValue(generator = "emp_id")
	@Id
	Integer id;

	@Column(name = "name")
	String empName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

}
