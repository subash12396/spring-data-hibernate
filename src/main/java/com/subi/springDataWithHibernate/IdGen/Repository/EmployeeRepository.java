package com.subi.springDataWithHibernate.IdGen.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.IdGen.Entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
