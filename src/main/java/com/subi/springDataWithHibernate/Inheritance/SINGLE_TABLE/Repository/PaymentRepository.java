package com.subi.springDataWithHibernate.Inheritance.SINGLE_TABLE.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.Inheritance.SINGLE_TABLE.Entity.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {

}
