package com.subi.springDataWithHibernate.Inheritance.JOINED.Entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "bankcheckjoined")
@PrimaryKeyJoinColumn(name = "id")
public class BankCheckJoined extends PaymentJoined {

	private String checknumber;

	public String getChecknumbe() {
		return checknumber;
	}

	public void setChecknumbe(String checknumber) {
		this.checknumber = checknumber;
	}

}
