package com.subi.springDataWithHibernate.Inheritance.JOINED.Entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "cardjoined")
@PrimaryKeyJoinColumn(name = "id")
public class CardJoined extends PaymentJoined {

	private String cardnumber;

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

}
