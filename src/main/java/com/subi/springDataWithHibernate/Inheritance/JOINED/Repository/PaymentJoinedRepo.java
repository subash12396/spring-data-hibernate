package com.subi.springDataWithHibernate.Inheritance.JOINED.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.Inheritance.JOINED.Entity.PaymentJoined;

public interface PaymentJoinedRepo extends CrudRepository<PaymentJoined, Integer> {

}
