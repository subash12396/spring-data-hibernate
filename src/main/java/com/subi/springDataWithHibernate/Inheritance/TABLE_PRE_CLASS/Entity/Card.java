package com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Entity;

import javax.persistence.Entity;

@Entity
public class Card extends PaymentTable {

	private String cardnumber;

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

}
