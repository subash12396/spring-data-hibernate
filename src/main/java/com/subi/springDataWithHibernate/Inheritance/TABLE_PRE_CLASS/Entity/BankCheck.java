package com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "bankcheck")
public class BankCheck extends PaymentTable {

	private String checknumber;

	public String getChecknumber() {
		return checknumber;
	}

	public void setChecknumber(String checknumber) {
		this.checknumber = checknumber;
	}

}
