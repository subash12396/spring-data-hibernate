package com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Entity.PaymentTable;

public interface PaymentRepo extends CrudRepository<PaymentTable, Integer>{

}
