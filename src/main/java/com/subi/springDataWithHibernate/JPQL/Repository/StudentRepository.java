package com.subi.springDataWithHibernate.JPQL.Repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.subi.springDataWithHibernate.JPQL.Entity.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {

	@Query("From Student")
	List<Student> findAllStudent();

	@Query("Select st.firstName, st.lastName from Student st")
	List<Object[]> findAllPartial();

	@Query("From Student st where st.score>:min and st.score<:max")
	List<Student> findAllByScores(@Param("min") int min, @Param("max") int max);

	@Modifying
	@Query("delete from Student st where st.firstName= :fName")
	void deleteStudentByName(@Param("fName") String name);

	@Query("From Student")
	List<Student> findAllStudentWithPagable(Pageable pageable);

	@Query("From Student")
	List<Student> findAllStudentWithPagableAndSort(Pageable pageable);

	@Query(value = "Select * from student", nativeQuery = true)
	List<Student> findAllStudentNative();

	@Query(value = "Select * from student where fname=:firstName", nativeQuery = true)
	List<Student> findAllByNameNQ(@Param("firstName") String firstName);

}
