package com.subi.springDataWithHibernate.ComponentMapping.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.ComponentMapping.Entity.EmployeeComponent;

public interface EmployeeRepo extends CrudRepository<EmployeeComponent, Integer> {

}
