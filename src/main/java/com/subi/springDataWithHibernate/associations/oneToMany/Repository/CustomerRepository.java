package com.subi.springDataWithHibernate.associations.oneToMany.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.associations.oneToMany.Entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
