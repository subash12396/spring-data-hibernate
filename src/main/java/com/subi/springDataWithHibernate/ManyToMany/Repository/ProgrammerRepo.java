package com.subi.springDataWithHibernate.ManyToMany.Repository;

import org.springframework.data.repository.CrudRepository;

import com.subi.springDataWithHibernate.ManyToMany.Entity.Programmer;

public interface ProgrammerRepo extends CrudRepository<Programmer, Integer> {

}
