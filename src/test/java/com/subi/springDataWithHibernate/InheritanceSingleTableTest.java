package com.subi.springDataWithHibernate;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.Inheritance.SINGLE_TABLE.Entity.Check;
import com.subi.springDataWithHibernate.Inheritance.SINGLE_TABLE.Entity.CreditCard;
import com.subi.springDataWithHibernate.Inheritance.SINGLE_TABLE.Repository.PaymentRepository;

@SpringBootTest
public class InheritanceSingleTableTest {

	@Autowired
	PaymentRepository paymentRepository;
	
	@Test
	public void testCreateCardPayment() {
		CreditCard cc = new CreditCard();
		cc.setAmount(33.44d);
		cc.setId(122);
		cc.setCardnumber("6373748858");
		paymentRepository.save(cc);	
	}
	
	@Test
	public void testCreateCheckPayment() {
		Check c = new Check();
		c.setAmount(4343.23d);
		c.setId(5453);
		c.setChecknumber("565645");
		paymentRepository.save(c);	
	}
}
