package com.subi.springDataWithHibernate;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.subi.springDataWithHibernate.Finder.Entity.Item;
import com.subi.springDataWithHibernate.Finder.Repository.ItemRepositiry;

@SpringBootTest
public class FinderMethodsTest {

	@Autowired
	ItemRepositiry itemRepo;

	@Test
	public void findAllItemByDesc() {
		List<Item> itemList = itemRepo.findByDescription("Apple Mobile");
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findAllItemByPrice() {
		List<Item> itemList = itemRepo.findByPrice((double) 400);
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findAllItemByNameAndPrice() {
		List<Item> itemList = itemRepo.findByNameAndPrice("Iphone 9s", (double) 400);
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findByPriceGreaterThan() {
		List<Item> itemList = itemRepo.findByPriceGreaterThan((double) 200);
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findByDescContains() {
		List<Item> itemList = itemRepo.findByDescriptionContains("Mobile");
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findByPriceBetween() {
		List<Item> itemList = itemRepo.findByPriceBetween((double) 100, (double) 300);
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findByNameLike() {
		List<Item> itemList = itemRepo.findByNameLike("%R%");
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

	/*
	 * @Test public void findByIdIn() { List<Integer> ids = Arrays.asList(102, 212,
	 * 104); List<Item> itemList = itemRepo.findByIdIn(ids); itemList.forEach((item)
	 * -> System.out.println(item.toString())); }
	 */

	/**
	 * Second page
	 * 
	 */
	@Test
	public void findByPageableSecondPage() {
		Pageable pageable = PageRequest.of(1, 2);
		Page<Item> items = itemRepo.findAll(pageable);
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * First Page
	 * 
	 */
	@Test
	public void findByPageableFirstPage() {
		Pageable pageable = PageRequest.of(0, 2);
		Page<Item> items = itemRepo.findAll(pageable);
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * Default direction is Ascending order
	 * 
	 */
	@Test
	public void sortByPrice() {
		Iterable<Item> items = itemRepo.findAll(Sort.by("price"));
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * Sort by Desc
	 * 
	 */
	@Test
	public void sortByPriceByDesc() {
		Iterable<Item> items = itemRepo.findAll(Sort.by(Direction.DESC, "price"));
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * Default direction is Ascending order
	 * 
	 */
	@Test
	public void sortByMultiple() {
		Iterable<Item> items = itemRepo.findAll(Sort.by("name", "price"));
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * Multiple direction and Multiple properties
	 * 
	 */
	@Test
	public void sortByMultipleAndDirection() {
		Iterable<Item> items = itemRepo.findAll(Sort.by(new Order(Direction.DESC, "name"), Order.by("price")));
		items.forEach((item) -> System.out.println(item.toString()));
	}

	/**
	 * Sort and Page
	 * 
	 */
	@Test
	public void findByPageableWithSort() {
		Pageable pageable = PageRequest.of(0, 2, Sort.by("name"));
		Page<Item> items = itemRepo.findAll(pageable);
		items.forEach((item) -> System.out.println(item.toString()));
	}

	@Test
	public void findByIdInWithPageable() {
		List<Integer> ids = Arrays.asList(102, 101, 103, 104);
		Pageable pageable = PageRequest.of(0, 2);
		List<Item> itemList = itemRepo.findByIdIn(ids, pageable);
		itemList.forEach((item) -> System.out.println(item.toString()));
	}

}
