package com.subi.springDataWithHibernate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Entity.BankCheck;
import com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Entity.Card;
import com.subi.springDataWithHibernate.Inheritance.TABLE_PRE_CLASS.Repository.PaymentRepo;

@SpringBootTest
public class InheritanceTablePreClassTest {

	@Autowired
	PaymentRepo paymentRepo;
	
	@Test
	public void testCreateWithCard() {
		Card c = new Card();
		c.setAmount(323.1);
		c.setCardnumber("54344");
		c.setId(1);
		paymentRepo.save(c);
 	}
	
	@Test
	public void testCreateForCheck() {
		BankCheck bc = new BankCheck();
		bc.setAmount(343.22);
		bc.setId(4323);
		bc.setChecknumber("456456456");
		paymentRepo.save(bc);
	}
	
}
