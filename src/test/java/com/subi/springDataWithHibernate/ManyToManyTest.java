package com.subi.springDataWithHibernate;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.subi.springDataWithHibernate.ManyToMany.Entity.Programmer;
import com.subi.springDataWithHibernate.ManyToMany.Entity.Project;
import com.subi.springDataWithHibernate.ManyToMany.Repository.ProgrammerRepo;

@SpringBootTest
public class ManyToManyTest {

	@Autowired
	private ProgrammerRepo repo;
	
	@Test
	public void testCreateProgrammer() {
		Programmer pro = new Programmer();
		pro.setName("Subash");
		pro.setSal(34234);
		Set<Project> projects = new HashSet<>();
		Project p1 = new Project();
		p1.setName("Hibernate");
		projects.add(p1);
		pro.setProjects(projects);
		repo.save(pro);
	}
	
	@Test
	@Transactional
	public void testFetchProgrammer() {
		Programmer pro = repo.findById(1).get();
		System.out.println(pro.getName());
		Set<Project> projects = pro.getProjects();
		for(Project project: projects) {
			System.out.println(project);
		}
		System.out.println();
	}
}
