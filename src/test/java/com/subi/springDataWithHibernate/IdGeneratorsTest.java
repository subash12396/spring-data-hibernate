package com.subi.springDataWithHibernate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.IdGen.Entity.Employee;
import com.subi.springDataWithHibernate.IdGen.Repository.EmployeeRepository;

@SpringBootTest
public class IdGeneratorsTest {

	@Autowired
	EmployeeRepository employeeRespository;
	
	@Test
	public void testEmployee() {
		Employee employee = new Employee();
		employee.setEmpName("Subi");
		employeeRespository.save(employee);
	}
	
	
}
