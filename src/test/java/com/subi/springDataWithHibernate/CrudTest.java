package com.subi.springDataWithHibernate;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.CRUD.Entity.Product;
import com.subi.springDataWithHibernate.CRUD.Repository.ProductRepository;

@SpringBootTest
public class CrudTest {

	@Autowired
	ProductRepository productRepository;

	@Test
	public void createProduct() {
		Product product = new Product();
		product.setId(101);
		product.setName("Samsung S20");
		product.setDescription("Mobile");
		product.setPrice(80000.00);
		productRepository.save(product);
	}

	@Test
	public void read() {
		Optional<Product> product = productRepository.findById(101);
		if (product.isPresent()) {
			System.out.println(product.get().toString());
		}
	}

	@Test
	public void readAll() {
		Iterable<Product> products = productRepository.findAll();
		products.forEach(product ->{
			System.out.println(product.toString());
		});
	}
	
	@Test
	public void update() {
		Optional<Product> product = productRepository.findById(101);
		if (product.isPresent()) {
			product.get().setName("S20 Ultra");
			productRepository.save(product.get());
		}
	}

	@Test
	public void delete() {
		Optional<Product> product = productRepository.findById(101);
		if (product.isPresent()) {
			productRepository.delete(product.get());
		}
	}
	
	@Test
	public void checkBefordDelete() {
		if (productRepository.existsById(101)) {
			productRepository.deleteById(101);
		}
	}
	
	@Test
	public void count() {
		Long totalRecords = productRepository.count();
		System.out.println(totalRecords);
	}
	
}
