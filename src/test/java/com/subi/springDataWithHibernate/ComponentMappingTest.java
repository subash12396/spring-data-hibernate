package com.subi.springDataWithHibernate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.ComponentMapping.Entity.Address;
import com.subi.springDataWithHibernate.ComponentMapping.Entity.EmployeeComponent;
import com.subi.springDataWithHibernate.ComponentMapping.Repository.EmployeeRepo;

@SpringBootTest
public class ComponentMappingTest {

	@Autowired
	EmployeeRepo repo;

	@Test
	public void testCreate() {
		EmployeeComponent e = new EmployeeComponent();
		e.setId(12);
		e.setName("Subi");
		Address address = new Address();
		address.setCity("Bangalore");
		address.setCountry("IN");
		address.setState("KA");
		address.setStreetaddress("SG Palya");
		address.setZipcode("537281");
		e.setAddress(address);
		repo.save(e);

	}
}
