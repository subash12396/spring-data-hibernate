package com.subi.springDataWithHibernate;

import java.awt.Label;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.OneToOne.Entity.License;
import com.subi.springDataWithHibernate.OneToOne.Entity.Person;
import com.subi.springDataWithHibernate.OneToOne.Repository.LicenseRepo;

@SpringBootTest
public class OneToOneTest {

	@Autowired
	private LicenseRepo repo;

	@Test
	public void testCreate() {
		License license = new License();
		license.setType("HDV");
		license.setValidFrom(new Date());
		license.setValidTo(new Date());
		
		Person person = new Person();
		person.setAge(34);
		person.setFirstName("subash");
		person.setLastName("s");
		
		license.setPerson(person);
		repo.save(license);
	}
}
