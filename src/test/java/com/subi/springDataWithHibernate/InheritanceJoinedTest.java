package com.subi.springDataWithHibernate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.subi.springDataWithHibernate.Inheritance.JOINED.Entity.BankCheckJoined;
import com.subi.springDataWithHibernate.Inheritance.JOINED.Entity.CardJoined;
import com.subi.springDataWithHibernate.Inheritance.JOINED.Repository.PaymentJoinedRepo;

@SpringBootTest
public class InheritanceJoinedTest {

	@Autowired
	PaymentJoinedRepo repo;

	@Test
	public void testCreateForCard() {
		CardJoined c = new CardJoined();
		c.setAmount(232.23213);
		c.setCardnumber("342345345");
		repo.save(c);
	}

	@Test
	public void testCreateForCheck() {
		BankCheckJoined bc = new BankCheckJoined();
		bc.setAmount(43234.44);
		bc.setChecknumbe("43234");
		repo.save(bc);

	}
}
