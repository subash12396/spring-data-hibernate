package com.subi.springDataWithHibernate;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.subi.springDataWithHibernate.JPQL.Entity.Student;
import com.subi.springDataWithHibernate.JPQL.Repository.StudentRepository;

@SpringBootTest
public class JPQLTest {

	@Autowired
	StudentRepository studentRepo;

	@Test
	public void insertData() {
		Student student = new Student();
		student.setFirstName("Subi");
		student.setLastName("Sh");
		student.setScore(89);
		studentRepo.save(student);

		student = new Student();
		student.setFirstName("Shasi");
		student.setLastName("Reddy");
		student.setScore(92);
		studentRepo.save(student);
	}

	@Test
	public void findAll() {
		List<Student> studentList = studentRepo.findAllStudent();
		studentList.forEach((s) -> System.out.println(s.toString()));
	}

	@Test
	public void findAllPartial() {
		List<Object[]> partialData = studentRepo.findAllPartial();
		for (Object[] object : partialData) {
			System.out.println(object[0]);
			System.out.println(object[1]);
		}
	}

	@Test
	public void findAllByScore() {
		List<Student> studentList = studentRepo.findAllByScores(80, 90);
		studentList.forEach((s) -> System.out.println(s.toString()));
	}

	@Transactional
	@Test
	@Rollback(false)
	public void deleteStudent() {
		studentRepo.deleteStudentByName("Subi");
	}

	@Test
	public void findAllWithPage() {
		Pageable pageable = PageRequest.of(1, 2);
		List<Student> studentList = studentRepo.findAllStudentWithPagable(pageable);
		studentList.forEach((s) -> System.out.println(s.toString()));
	}

	@Test
	public void findAllWithPageAndSort() {
		Sort sort = Sort.by(Direction.DESC, "score");
		Pageable pageable = PageRequest.of(0, 3, sort);
		List<Student> studentList = studentRepo.findAllStudentWithPagableAndSort(pageable);
		studentList.forEach((s) -> System.out.println(s.toString()));
	}
	
	@Test
	public void findAllNative() {
		List<Student> studentList = studentRepo.findAllStudentNative();
		studentList.forEach((s) -> System.out.println(s.toString()));
	}
	
	@Test
	public void findFirstNameNative() {
		List<Student> studentList = studentRepo.findAllByNameNQ("Subi");
		studentList.forEach((s) -> System.out.println(s.toString()));
	}
	
}
