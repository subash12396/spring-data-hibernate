package com.subi.springDataWithHibernate;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.subi.springDataWithHibernate.associations.oneToMany.Entity.Customer;
import com.subi.springDataWithHibernate.associations.oneToMany.Entity.PhoneNumber;
import com.subi.springDataWithHibernate.associations.oneToMany.Repository.CustomerRepository;

@SpringBootTest
public class OneToManyAndManyToOneTest {

	@Autowired
	private CustomerRepository customerRepo;

	@Test
	public void createCustomer() {
		Customer customer = new Customer();
		customer.setName("Subi");
		Set<PhoneNumber> phoneNumberSet = new HashSet<PhoneNumber>();

		// Mobile
		PhoneNumber ph1 = new PhoneNumber();
		ph1.setNumber("9566878553");
		ph1.setType("Mobile");

		// office
		PhoneNumber ph2 = new PhoneNumber();
		ph2.setNumber("865864556");
		ph2.setType("office");
		phoneNumberSet.add(ph1);
		phoneNumberSet.add(ph2);
		customer.setPhoneNumber(phoneNumberSet);
		customerRepo.save(customer);
	}

	@Test
	public void createCustomerWithAddPhoneNumber() {
		Customer customer = new Customer();
		customer.setName("Subi");

		// Mobile
		PhoneNumber ph1 = new PhoneNumber();
		ph1.setNumber("9566878553");
		ph1.setType("Mobile");

		// office
		PhoneNumber ph2 = new PhoneNumber();
		ph2.setNumber("865864556");
		ph2.setType("office");
		customer.addPhoneNumber(ph1);
		customer.addPhoneNumber(ph2);

		customerRepo.save(customer);
	}

	@Test
	@Transactional
	public void fetchCustomer() {
		Customer customer = customerRepo.findById(3).get();
		System.out.println(customer.getName());
		Set<PhoneNumber> numbers = customer.getPhoneNumber();
		for (PhoneNumber phoneNumber : numbers) {
			System.out.println(phoneNumber.getType());
			System.out.println(phoneNumber.getNumber());
		}
	}
	
	@Test
	public void updateCustomer() {
		Customer customer = customerRepo.findById(3).get();
		customer.setName("subash s");
		Set<PhoneNumber> numbers = customer.getPhoneNumber();
		for (PhoneNumber phoneNumber : numbers) {
			phoneNumber.setType("mobile");
		}
		customerRepo.save(customer);
	}
	
	@Test
	public void testDelete() {
		customerRepo.deleteById(3);
	}

}
